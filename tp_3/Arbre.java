import java.util.*;

class Arbre<T> {

  protected T valeur;
  protected Arbre<T> filsGauche, filsDroit;

  public T valeur() {
    return valeur;
  }

  public boolean existeFilsGauche() {
    return filsGauche != null;
  }

  public boolean existeFilsDroit() {
    return filsDroit != null;
  }

  public Arbre<T> filsGauche() {
    return filsGauche;
  }

  public Arbre<T> filsDroit() {
    return filsDroit;
  }

  public void affecterValeur(T c) {
    valeur = c;
  }

  public void affecterFilsGauche(Arbre<T> g) {
    filsGauche = g;
  }

  public void affecterFilsDroit(Arbre<T> d) {
    filsDroit = d;
  }

  public boolean feuille() {
    return (filsDroit == null && filsGauche == null);
  }

  public int hauteur() {
    int g = existeFilsGauche() ? filsGauche.hauteur() : 0;
    int d = existeFilsDroit() ? filsDroit.hauteur() : 0;
    return 1 + Math.max(g, d);
  }

  // Constructeurs
  public Arbre(T val) {
    valeur = val;
    filsGauche = filsDroit = null;
  }

  public Arbre(T val, Arbre<T> g, Arbre<T> d) {
    valeur = val;
    filsGauche = g;
    filsDroit = d;
  }

  // Affichage
  public void afficherPrefixe() {
    System.out.print(valeur + "\t");
    if (existeFilsGauche()) filsGauche.afficherPrefixe();
    if (existeFilsDroit()) filsDroit.afficherPrefixe();
  }

  public void afficherInfixe() {
    if (existeFilsGauche()) filsGauche.afficherInfixe();
    System.out.print(valeur + "\t");
    if (existeFilsDroit()) filsDroit.afficherInfixe();
  }

  public void afficherPostfixe() {
    if (existeFilsGauche()) filsGauche.afficherPostfixe();
    if (existeFilsDroit()) filsDroit.afficherPostfixe();
    System.out.print(valeur + "\t");
  }

  @SuppressWarnings("unchecked")
  private void move(Arbre<?> a, boolean setNull) {
    valeur = (T) a.valeur;
    filsGauche =
      !setNull && a.existeFilsGauche() ? (Arbre<T>) a.filsGauche : null;
    filsDroit = !setNull && a.existeFilsDroit() ? (Arbre<T>) a.filsDroit : null;
  }

  private void move(Arbre<?> a) {
    move(a, false);
  }

  private static boolean isNumeric(String strNum) {
    if (strNum == null) {
      return false;
    }
    try {
      double d = Double.parseDouble(strNum);
    } catch (NumberFormatException nfe) {
      return false;
    }
    return true;
  }

  private boolean existAndEquals(Arbre<?> a, String s, boolean isLeft) {
    if (isLeft) {
      return (a.existeFilsGauche() && a.filsGauche.valeur.toString().equals(s));
    }
    return (a.existeFilsDroit() && a.filsDroit.valeur.toString().equals(s));
  }

  private boolean existAndEquals(Arbre<?> a, String s) {
    return existAndEquals(a, s, true);
  }

  @SuppressWarnings("unchecked")
  private int caseFils(Arbre<?> a) {
    a = (Arbre<T>) a;
    if (
      isNumeric(a.filsGauche.valeur.toString()) &&
      isNumeric(a.filsDroit.valeur.toString())
    ) {
      return 0;
    } else if (
      isNumeric(a.filsGauche.valeur.toString()) &&
      a.filsDroit.valeur.toString().equals("x")
    ) {
      return 1;
    } else if (
      a.filsGauche.valeur.toString().equals("x") &&
      isNumeric(a.filsDroit.valeur.toString())
    ) {
      return 2;
    } else if (
      a.filsGauche.valeur.toString().equals("x") &&
      a.filsDroit.valeur.toString().equals("x")
    ) {
      return 3;
    } else {
      return -1;
    }
  }

  public void simplify() {
    // A + 0 -> A
    // A * 0 -> 0
    // A * 1 -> A
    // A ^ 1 -> A
    // A ^ 0 -> 1
    // A - 0 -> A
    // A / 1 -> A

    if (existeFilsGauche()) filsGauche.simplify();
    if (existeFilsDroit()) filsDroit.simplify();
    if (valeur.toString().equals("+")) {
      if (existAndEquals(this, "0")) {
        move(filsDroit);
      } else if (existAndEquals(this, "0", false)) {
        move(filsGauche);
      }
    } else if (valeur.toString().equals("-")) {
      if (existAndEquals(this, "0", true)) {
        move(filsGauche);
      }
    } else if (valeur.toString().equals("*")) {
      if (existAndEquals(this, "0")) {
        move(filsGauche, true);
      } else if (existAndEquals(this, "0", false)) {
        move(filsDroit, true);
      } else if (existAndEquals(this, "1")) {
        move(filsDroit);
      } else if (existAndEquals(this, "1", false)) {
        move(filsGauche);
      }
    } else if (valeur.toString().equals("/")) {
      if (existAndEquals(this, "1", false)) {
        move(filsGauche);
      }
    } else if (valeur.toString().equals("^")) {
      if (existAndEquals(this, "1", false)) {
        move(filsGauche, true);
      } else if (existAndEquals(this, "0", false)) {
        move(new Arbre<Integer>(1));
      }
    }
  }

  public void prime() {
    // example: (x^2 + 2x + 1)' = 2x + 2
    // polynome: (a*x^n + b*x^m + c*x^o + ...)'
    // use: java Sc "(((x^2)+(2*x))+1)"
    if (this.feuille()) return;
    if (!(filsDroit.feuille() && filsGauche.feuille())) {
      filsDroit.prime();
      filsGauche.prime();
      return;
    }
    if (valeur.toString().equals("+")) {
      switch (caseFils(this)) {
        case 0:
          move(new Arbre<Integer>(0));
          break;
        case 1:
        case 2:
          move(new Arbre<Integer>(1));
          break;
        case 3:
          move(new Arbre<Integer>(2));
          break;
        default:
          throw new UnsupportedOperationException(
            "Input Error" +
            filsGauche.valeur.toString() +
            " + " +
            filsDroit.valeur.toString()
          );
      }
    } else if (valeur.toString().equals("*")) {
      switch (caseFils(this)) {
        case 0:
          move(new Arbre<Integer>(0));
          break;
        case 1:
          move(new Arbre<String>(filsGauche.valeur.toString()));
          break;
        case 2:
          move(new Arbre<String>(filsDroit.valeur.toString()));
          break;
        case 3:
          // move(construireArbre(new String[] { "2", "*", "x" }));
          break;
        default:
          throw new UnsupportedOperationException(
            "Input Error" +
            filsGauche.valeur.toString() +
            " + " +
            filsDroit.valeur.toString()
          );
      }
    } else if (valeur.toString().equals("^")) {
      switch (caseFils(this)) {
        case 0:
          move(new Arbre<Integer>(0));
          break;
        case 1:
          throw new UnsupportedOperationException(
            "Not a polynome: " + filsGauche.valeur.toString() + "^x"
          );
        case 2:
          String d = "1";
          Arbre<String> r = new Arbre<String>(
            "^",
            new Arbre<String>("x"),
            new Arbre<String>(Integer.toString(Integer.parseInt(d) - 1))
          );
          move(
            new Arbre<String>(
              "*",
              new Arbre<String>(d),
              r
            )
          );
          break;
        case 3:
          throw new UnsupportedOperationException("Not a polynome: x^x");
        default:
          throw new UnsupportedOperationException(
            "Input Error" +
            filsGauche.valeur.toString() +
            " + " +
            filsDroit.valeur.toString()
          );
      }
    }
  }

  public Double calcul() {
    if (feuille()) {
      return Double.parseDouble(valeur.toString());
    } else {
      double g = existeFilsGauche() ? filsGauche.calcul() : 0;
      double d = existeFilsDroit() ? filsDroit.calcul() : 0;
      switch (valeur.toString()) {
        case "+":
          return g + d;
        case "-":
          return g - d;
        case "*":
          return g * d;
        case "/":
          return g / d;
        case "^":
          return Math.pow(g, d);
        default:
          return 0.0;
      }
    }
  }
}
