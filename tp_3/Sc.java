// Exemple d'utilisation de la classe Arbre fournie pour le TP n III
// exemple d'execution : java Sc "(x+3)*(5-(2*x))"
// necessite les fichiers Arbre.java, Clavier.javaGUIHelper.java, JCanvas.java, ManipulationArbre.java, TextInter.java
import java.awt.Dimension;
import java.io.*;

public class Sc {

  public static void main(String args[]) throws IOException {
    //utilisation des methodes pour transformer le texte d'une expression en un arbre (avec stockage dans un format intermediaire)
    if (args.length == 0) {
      System.out.println("Default");
      ManipulationArbre.generationXMLdepuisTexte("(x+3)*(5-(2*x))", "XML.xml");
    } else if (args.length == 2) {
      System.out.println("Calcul");
      String exp = args[0].replace("x", args[1]);
      ManipulationArbre.generationXMLdepuisTexte(exp, "XML.xml");
    } else {
      System.out.println("Normal");
      ManipulationArbre.generationXMLdepuisTexte(args, "XML.xml");
    }
    Arbre arbre = ManipulationArbre.XMLVersArbre("XML.xml");
    arbre.afficherInfixe();
    System.out.println();
    // arbre.simplify();
    // arbre.afficherInfixe();
    // System.out.println();
    arbre.calcul();
    arbre.afficherInfixe();

    // arbre.prime();
    // arbre.afficherInfixe();
    // System.out.println();
    // arbre.simplify();
    // arbre.afficherInfixe();
    // System.out.println();
    // System.out.println(arbre.calcul());
    //utilisation de la methode pour afficher l'arbre dans une fenetre.
    // Arbre arbre = new Arbre<String>(new String[]{"D", "B", "E", "A", "F", "C"});
    arbre.afficherInfixe();
    // ManipulationArbre.Affiche(arbre);
  }
}
