public class PersonneMariee extends Personne{
    protected Personne conjoint;

    public PersonneMariee(String nom, String prenom, int anneNaissance, Boolean sexe) {
        super(nom, prenom, anneNaissance, sexe);
        this.conjoint = null;
    }

    public PersonneMariee(String nom, String prenom, int anneNaissance, Boolean sexe, Personne conjoint) {
        super(nom, prenom, anneNaissance, sexe);
        conjoint = new PersonneMariee(conjoint, this);
        this.conjoint = conjoint;
    }

    public PersonneMariee(Personne p) {
        super(p);
        this.conjoint = null;
    }

    public PersonneMariee(Personne p, Personne conjoint) {
        super(p);
        this.conjoint = conjoint;
    }

    public Boolean marier(Personne p) {
        Boolean eq = p.nom == this.nom && p.prenom == this.prenom && p.anneNaissance == this.anneNaissance;
        return p.sexe != this.sexe && !eq;
    }

    public String toString() {
        String verbe = this.sexe ? "marié" : "mariée";
        return super.toString() + " " + (this.conjoint != null ? "est " + verbe + " avec " + this.conjoint.nom + " " + this.conjoint.prenom + "." : "n'est pas " + verbe + ".");
    }

}
