public class Personne {
    protected String nom;
    protected String prenom;
    protected int anneNaissance;
    protected Boolean sexe;
    protected Personne mere;
    protected Personne pere;

    public Personne(String nom, String prenom, int anneNaissance, Boolean sexe) {
        this.nom = nom;
        this.prenom = prenom.toUpperCase();
        this.anneNaissance = anneNaissance;
        this.sexe = sexe;
        this.mere = null;
        this.pere = null;
    }

    public Personne(Personne p) {
        this(p.nom, p.prenom, p.anneNaissance, p.sexe);
    }

    public int getAge() {
        int age = 2013 - this.anneNaissance;
        return age >= 0 ? age : 0;
    }

    public void setParent(Personne parent) {
        if (parent.sexe)
            this.pere = parent;
        else
            this.mere = parent;
    }

    public String toString() {
        String sexe = this.sexe ? "Monsieur " : "Madame ";
        String verbe = this.sexe ? "né" : "née";
        if (this.pere == null && this.mere == null)
            return sexe + this.nom + " " + this.prenom + " est " + verbe + " en " + this.anneNaissance;
        return sexe + this.nom + " " + this.prenom + " est " + verbe + " en " + this.anneNaissance + ". Ses parents sont " + pere.nom + " et " + mere.nom + ".";
    }
}

