import java.util.Vector;

public class PersonneFamille extends PersonneMariee {
    protected Vector<Personne> enfants;

    public PersonneFamille(String nom, String prenom, int anneNaissance, Boolean sexe, Personne conjoint) {
        super(nom, prenom, anneNaissance, sexe, conjoint);
        this.enfants = new Vector<Personne>();
    }

    public PersonneFamille(Personne p) {
            super(p, ((PersonneMariee)p).conjoint);

        this.enfants = new Vector<Personne>();
    }

    public void ajouterEnfant(Personne p) {
        this.enfants.add(p);
        // if (this.conjoint instanceof PersonneMariee)
        //     ((PersonneFamille) this.conjoint).ajouterEnfant(p);

        p.setParent(this);
        p.setParent(this.conjoint);
    }

    public int getNumEnfants() {
        return this.enfants.size();
    }

    public String toString() {
        String res = super.toString() + " a " + this.getNumEnfants() + " enfant" + (this.getNumEnfants() > 1 ? "s" : "") + ":";
        for (Personne p : this.enfants) {
            res += "\n\t" + p.toString();
        }
        return res;
    }

}
