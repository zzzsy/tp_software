public class Test {
    // System.out.println(p.toString());

    public void testPersonne() {
        System.out.println("\n=== Test Personne ===");
        Personne p = new Personne("Dupont", "Jean", 1980, true);
        System.out.println(p);
        assert p.getAge() == 33;
        assert p.toString().equals("Monsieur Dupont JEAN est né en 1980");
        System.out.println("Test Personne: OK");
    }

    public void testPersonneMariee() {
        System.out.println("\n=== Test PersonneMariee ===");
        Personne p1 = new Personne("Dupont", "Jean", 1980, true);
        Personne p2 = new Personne("Dupont", "Jeanne", 1980, false);
        PersonneMariee pm1 = new PersonneMariee(p1, p2);
        System.out.println(pm1);
        assert pm1.marier(p1) == false;
        assert pm1.marier(p2) == true;
        System.out.println("Test PersonneMariee: OK");
    }

    public void testPersonneFamille() {
        System.out.println("\n=== Test PersonneFamille ===");
        Personne p1 = new Personne("MA", "Jean", 1980, true);
        Personne p2 = new Personne("MB", "Jeanne", 1980, false);
        Personne p3 = new Personne("A", "e1", 2000, true);
        Personne p4 = new Personne("B", "e2", 2001, false);

        PersonneMariee pm = new PersonneMariee(p1, p2);
        PersonneFamille pf = new PersonneFamille(pm);

        pf.ajouterEnfant(p3);
        pf.ajouterEnfant(p4);

        assert pf.getNumEnfants() == 2;
        System.out.println(pf);

        assert sontFreresSoeurs(p3, p4) == true;
        assert sontFreresSoeurs(p1, p2) == false;
        System.out.println("Test PersonneFamille: OK");
    }

    public Boolean sontFreresSoeurs(Personne p1, Personne p2) {
        return p1.mere == p2.mere && p1.pere == p2.pere && p1 != p2 && p1.mere != null && p1.pere != null;
    }

    public static void main(String[] args) {
        Test t = new Test();
        t.testPersonne();
        t.testPersonneMariee();
        t.testPersonneFamille();
    }
}
