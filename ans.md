# 1

## Q.1.1

```java
public Segment() {
    vectP = new Vector<Point>();
}
```

## Q.1.2

```java
public Segment(Segement seg) {
    vectP = new Vector<Point>(seg.vectP);
    for(Point p : seg.vectP) {
        this.vectP.add(new Point(p));
    }
}
```

## Q.1.3

```java
public Segment(Point p1, Point p2){
    vectP = new Vector<Point>();
    this.vectP.add(p1);
    this.vectP.add(p2);
}
```

## Q.1.4

```java
public void translater(int dx, int dy){
    for (Point p : vectP) {
        p.translater(dx, dy);
    }
}
```

## Q.1.5

```java
public Segment translaterS(int dx, int dy){
    for (Point p : vectP) {
        p.translater(dx, dy);
    }
    return new Segment(this);
}
```


# 2

## Q.2.1

```java
public int longueur(){
    if (this == null) return 0;
    else return 1 + this.successeur().longueur();
}
```

# 3

## Q.3.1

```
x: 7
Apple de f(x)
x: 8
```

# 4

## Q.4.1

```java
public int taille() {
    int g = existeFilsGauche() ? filsGauche.taille() : 0;
    int d = existeFilsDroit() ? filsDroit.taille() : 0;
    return 1 + g + d;
}
```

# 5

## Q.5.1

```java

public EnsEntiersNaturels() {
    contenu = new int[N_MAX];
    for (int i = 0; i < N_MAX; i++) {
        contenu[i] = CASE_VIDE;
    }
    nbElements = 0;
}

public boolean supprimer(int e) {
    for (int i = 0; i < N_MAX; i++) {
        if (contenu[i] == e) {
            contenu[i] = CASE_VIDE;
            nbElements--;
            return true;
        }
    }
    return false;
}

public int cardinal() {
    return nbElements;
}

public boolean contient(int e) {
    for (int i = 0; i < N_MAX; i++) {
        if (contenu[i] == e) {
            return true;
        }
    }
    return false;
}
```
## Q.5.2

```java
public EnsEntiersNaturels intersection(EnsEntiersNaturels e) {
    EnsEntiersNaturels res = new EnsEntiersNaturels();
    for (int i = 0; i < N_MAX; i++) {
        if (contient(contenu[i]) && e.contient(contenu[i])) {
            res.ajouter(contenu[i]);
        }
    }
    return res;
}

public EnsEntiersNaturels union(EnsEntiersNaturels e) {
    EnsEntiersNaturels res = new EnsEntiersNaturels();
    for (int i = 0; i < N_MAX; i++) {
        res.ajouter(contenu[i]);
    }
    for (int i = 0; i < N_MAX; i++) {
        if (!res.contient(e.contenu[i])) {
            res.ajouter(e.contenu[i]);
        }
    }
    return res;
}
```

# 6

## Q.6.1

```java
public class Personne {
    private String nom;
    private String prenom;
    private Byte[] num;

    public Personne(String nom, String prenom, Byte[] num) {
        this.nom = nom;
        this.prenom = prenom;
        if (num != null && num.length == 10) {
            this.num = num;
        } else {
            this.num = new Byte[10];
        }
    }
}

```

## Q.6.2

> idk the Type of return value

> is it `void` or `String`?

```java
public void leNumero() {
    System.out.print(nom + " " + prenom + " : ");
    afficheTab10Bytes(num);
}
```

## Q.6.3

```java
public class PersonneAvecRepertoire extends Personne {
    private List<Personne> repertoire;

    public PersonneAvecRepertoire(String nom, String prenom, Byte[] num) {
        super(nom, prenom, num);
        this.repertoire = new ArrayList<Personne>();
    }
}
```

## Q.6.4

```java
public void ajouterPersonne(Personne p) {
    if (p != null && !repertoire.contains(p)) {
        repertoire.add(p);
    }
}
```

## Q.6.5

```java
public void personne(String nom) {
    for (Personne p : repertoire) {
        if (p.getNom().equals(nom)) {
            p.leNumero();
            return;
        }
    }
}
```

## Q.6.6

```java
public int combien() {
    int cnt = 0;
    for (Personne p : repertoire) {
        if (p instanceof PersonneAvecRepertoire) {
            for (Personne p2 : ((PersonneAvecRepertoire) p).repertoire) {
                if (p2.getNom().equals(this.getNom()) && p2.getPrenom().equals(this.getPrenom())) {
                    cnt++;
                }
            }
        }
        return cnt;
    }
}
