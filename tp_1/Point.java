//Classe Point

public class Point {
	protected int x;
	protected int y;
	
	public Point(int a,int b) {
		this.x = a;
		this.y = b;
	}
	
	public Point(Point p) {
		this.x = p.x;
		this.y = p.y;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public void translater(int a,int b) {
		// change les coordonnées du point
		this.x += a;
		this.y += b;
	}
	public Point translaterP(int a,int b) {
		return new Point(this.x+a, this.y+b);
	}
	public String toString() {
		return("abscisse : "+x+", ordonnée : "+y);
	}
}
		
	
	
