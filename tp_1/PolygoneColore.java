// Class Segment

import java.awt.* ;
import java.util.Vector;

public class PolygoneColore extends Polygone {
	private Color color;
	
	public PolygoneColore(){
		vectP = new Vector<Point>();
	}

	public PolygoneColore(Polygone poly, Color c){
		super(poly);
		color = c;
	}
	
	public void colorerObjet(Graphics g){
		g.setColor(color);
	}

	public void dessinerObjet(Graphics g){
        for(int i = 1; i < vectP.size(); i++){
            ligne(g, vectP.get(i-1).getX(),vectP.get(i-1).getY(), vectP.get(i).getX(),vectP.get(i).getY());
        }
        ligne(g, vectP.get(0).getX(),vectP.get(0).getY(), vectP.get(vectP.size()-1).getX(),vectP.get(vectP.size()-1).getY());
    }

}
		
		
