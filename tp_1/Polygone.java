// Class Segment

import java.awt.* ;
import java.util.Vector;

public class Polygone extends ObjetGraphique {
	protected Vector<Point> vectP;
	
	public Polygone(){
		vectP = new Vector<Point>();
	}

	public Polygone(Polygone poly){
		vectP = new Vector<Point>();
		for(Point p : poly.getVectP()){
			this.vectP.add(new Point(p));
		}
	}

	public Vector<Point> getVectP() {
		return vectP;
	}
		
	public void translater(int a, int b){
		for (Point p : vectP) {
			p.translater(a,b);
		}
	}
		
	public Polygone translaterPoly(int a, int b){
		for (Point p : vectP) {
			p.translater(a,b);
		}
		return new Polygone(this);
	}
		
	public void ajouterSommet(Point P2) {
		vectP.add(new Point(P2));
	}

	public void dessinerObjet(Graphics g){
        for(int i = 1; i < vectP.size(); i++){
            ligne(g, vectP.get(i-1).getX(),vectP.get(i-1).getY(), vectP.get(i).getX(),vectP.get(i).getY());
        }
        ligne(g, vectP.get(0).getX(),vectP.get(0).getY(), vectP.get(vectP.size()-1).getX(),vectP.get(vectP.size()-1).getY());
    }

}
		
		
