/**
   fichier Test.java
   Trace de differents objets graphiques
   Utilisation de la classe obstraite ObjetGraphique
*/

import java.util.*;
import java.awt.* ;
import javax.swing.* ;
import java.awt.event.* ;

public class Test1 {
   public static void main(String[] a) {       
	Vector<ObjetGraphique> mesObjetsG = new Vector<ObjetGraphique>();
	Polygone poly1 = new Polygone();
	       
	Point p1 = new Point(100,100);
	Point p2 = new Point(100,200);
	Point p3 = new Point(200,300);
	Point p4 = new Point(300,300);
      
	poly1.ajouterSommet(p1);
	poly1.ajouterSommet(p2);
	poly1.ajouterSommet(p3);
	poly1.ajouterSommet(p4);
	PolygoneColore pp1 = new PolygoneColore(new Polygone (poly1), Color.red);
	Polygone poly2 = new Polygone();
	poly2.ajouterSommet(p1);
	poly2.ajouterSommet(p2);
	poly2.ajouterSommet(p3);
	poly2.translater(150,-60);
	PolygoneColore pp2 = new PolygoneColore(new Polygone (poly2), Color.blue);
	// poly1.translater(150,-60);


      // Instructions d'affichage des figures
      Affichage monDessin;  monDessin = new Affichage();
      monDessin.ajoutObjet(pp1);
	  monDessin.ajoutObjet(pp2);
	  monDessin.setVisible(true);
   }
}


