///////////////////////////////////////////////////////
// Olivier ROUX
//  - TP2  	Tp.java
///////////////////////////////////////////////////////

import java.io.*;
import java.util.*;

public class Tp {

  public static void main(String[] argv) throws IOException {
    String fichier;
    fichier = (argv.length != 0) ? argv[0] : "texte";

	// long startTime1 = System.currentTimeMillis();
    // Contenu t = new Contenu(fichier);
    // t.ecri();
	// long stopTime1 = System.currentTimeMillis();
	// System.out.println("Time using Link: " + (stopTime1 - startTime1));

	long startTime2 = System.currentTimeMillis();
	Contenu2 t2 = new Contenu2(fichier);
	t2.print();
	long stopTime2 = System.currentTimeMillis();
	System.out.println("Time using HashMap: " + (stopTime2 - startTime2));
  }
}
