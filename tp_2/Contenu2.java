/*

D´efinissez et implantez en java les classes n´ecessaires pour effectuer au moins les op´erations suivantes
— constituer, `a partir d’un texte contenu dans un fichier d’entr´ee, un lexique des mots utilis´es avec le nombre d’occurrences pour chaque mot ;
— sauvegarder le contenu du lexique ainsi produit dans un fichier de sortie ;
— afficher le nombre de mots diff´erents pr´esents dans le lexique ;
— tester la pr´esence d’un mot dans le lexique et retourner son nombre d’occurences ;
— supprimer un mot du lexique
*/

import java.io.*;
import java.util.*;


public class Contenu2 extends HashMap<String, Integer> {

  protected String nom;

  public Contenu2(String texte) throws IOException {
    nom = texte;
    Reader reader = new FileReader(texte + ".txt");
    BufferedReader in = new BufferedReader(reader);
    String ligne = in.readLine();

    while (ligne != null) {
      String delimiteurs =
        "/0123456789~^%$*-_=+|#'.,;:?!() {}[]`\"\t\\<>“”''";
      StringTokenizer st = new StringTokenizer(ligne, delimiteurs);
      while (st.hasMoreTokens()) {
        String mot = st.nextToken();
        mot = mot.toLowerCase();
        // System.out.println(mot);
        this.add((String) mot);
      }
      ligne = in.readLine();
    }
  }

  public void add(String mot) {
    if (this.containsKey(mot)) {
      this.put(mot, this.get(mot) + 1);
    } else {
      this.put(mot, 1);
    }
  }

  public void remove(String mot) {
    if (this.containsKey(mot)) {
      this.remove(mot);
    }
  }

  public int getMot(String mot) {
    if (this.containsKey(mot)) {
      return this.get(mot);
    } else {
      return 0;
    }
  }

  public void print() {
    for (String mot : this.keySet()) {
      System.out.println(mot + " : " + this.get(mot));
    }
  }
}
